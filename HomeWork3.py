import csv

height = 0
weight = 0
length = 0

INCH_CM = 2.5
POUND_KG = 0.453592

with open('hw.csv', 'r') as file:
    reader = csv.reader(file)
    next(reader)
    try:
        for row in reader:
            height += float(row[1])
            weight += float(row[2])
            length += 1
    except IndexError:
        pass

    average_height = round(height / length * INCH_CM, 2)
    average_weight = round(weight / length * POUND_KG, 2)
    print(f'Средняя высота = {average_height} см, средний вес = {average_weight} кг')